package org.bryt.pages.compare;

import org.bryt.driver.WebDriverHolder;
import org.bryt.model.ProductModel;
import org.bryt.pages.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class CompareListPage extends BasePage {

    @FindBy(css = ".compare-products-table")
    private WebElement compareTable;


    public List<ProductModel> getProductsInfo() {
        sleep(2000);
        List<WebElement> productNamesWebElements = compareTable.findElements(By.cssSelector("tr.product-name a"));
        List<WebElement> productPricesWebElements = compareTable.findElements(By.cssSelector("tr.product-price td"));
        productPricesWebElements = productPricesWebElements.subList(1, productPricesWebElements.size());

        List<ProductModel> list = new ArrayList<>();

        for (int i = 0; i < productNamesWebElements.size(); i++) {
            ProductModel productModel = new ProductModel();
            productModel.productName(productNamesWebElements.get(i).getText().trim());
            String productPriceAsString = productPricesWebElements.get(i).getText();
            Double price = Double.parseDouble(productPriceAsString.substring(1).replaceAll(",", ""));
            productModel.price(price);
            list.add(productModel);
        }
        return list;
    }

    public CompareListPage clearList() {
        WebDriverHolder.getInstance().getDriver().findElement(By.cssSelector(".clear-list")).click();
        return new CompareListPage();
    }

}
