Feature: Compare items in shop

  As user of the system I'd like to have an ability to compare 2 or more items

  Background: Login
    Given User navigates to Login page
    And User log in

  Scenario: Compare 2 different notebooks
    Given User navigates to "Computers->Notebooks"
    And User add to compare list "Asus N551JK-XO076H Laptop"
    And User add to compare list "Lenovo Thinkpad X1 Carbon Laptop"
    When User go to compare list
    Then At compare list should be:
      | Asus N551JK-XO076H Laptop        |
      | Lenovo Thinkpad X1 Carbon Laptop |