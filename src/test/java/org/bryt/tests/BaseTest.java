package org.bryt.tests;

import org.bryt.driver.WebDriverHolder;
import org.bryt.utils.PropertyReader;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

    @BeforeSuite
    public void beforeClass() {
        WebDriverHolder.getInstance();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        WebDriverHolder.getInstance().quitDriver();
    }

    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/"))
            WebDriverHolder.getInstance().getDriver().get(baseUrl + url);
        else WebDriverHolder.getInstance().getDriver().get(url);
    }

    protected void openUrl() {
       openUrl(PropertyReader.getInstance().getProperty("baseUrl"));
    }
}
