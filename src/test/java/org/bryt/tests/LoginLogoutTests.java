package org.bryt.tests;

import org.bryt.pages.base.BasePage;
import org.bryt.utils.PropertyReader;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginLogoutTests extends BaseTest{

    @BeforeMethod
    public void beforeMethod(){
        openUrl();
    }

    @Test
    public void loginTest(){
        String userEmail = PropertyReader.getInstance().getProperty("defaultUser");
        String userPass = PropertyReader.getInstance().getProperty("userPass");

        BasePage basePage = new BasePage()
                .getMainMenu()
                .selectLogin()
                .login(userEmail, userPass);
        assertThat(basePage.getMainMenu().isLogOutMenuItemVisible())
                .as("Logaut menu item should be visible")
                .isTrue();
        assertThat(basePage.getMainMenu().isMyAccountMenuItemVisible())
                .as("MyAccount menu item should be visible")
                .isTrue();

        basePage
                .getMainMenu()
                .selectLogout();

    }
}
