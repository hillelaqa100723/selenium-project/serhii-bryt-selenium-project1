package org.bryt.pages.content.computers;

import org.bryt.model.ProductModel;
import org.bryt.pages.content.BaseContentPage;

import java.util.List;

public class NotebooksPage extends BaseContentPage {

    public List<ProductModel> getNotebooks() {
        return getProductAsList();
    }

    public NotebooksPage addNoteBookToCart(String noteBookName) {
        addItemToCard(noteBookName);
        return new NotebooksPage();
    }

    public NotebooksPage addNoteBookToCompare(String noteBookName) {
        addItemToCompare(noteBookName);
        sleep(2000);
        return new NotebooksPage();
    }

    public NotebooksPage addNoteBooksToCompare(List<String> noteBookNames) {
        for (String noteBookName : noteBookNames) {
            addNoteBookToCompare(noteBookName);
        }
        return new NotebooksPage();
    }

    public ProductModel getNoteBookInfo(String noteBookName){
        return getProductModelInfo(noteBookName);
    }


}
