package org.bryt.tests;

import org.bryt.model.ProductModel;
import org.bryt.model.ProductModelCart;
import org.bryt.model.enums.SortByDirection;
import org.bryt.pages.base.BasePage;
import org.bryt.pages.cart.CartPage;
import org.bryt.pages.content.computers.NotebooksPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class NoteBooksTests extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");
    }

    @Test
    public void getProducts() {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");
        List<ProductModel> notebooks = new NotebooksPage()
                .getNotebooks();

        System.out.println(notebooks);
    }

    @Test
    public void sortNoteBooks_A_ZTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage()
                .getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.NAME_Z_A);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.sort(ProductModel.getComparator(SortByDirection.NAME_Z_A));

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .isEqualTo(notebooksBeforeSort);
    }

    @Test
    public void sortNoteBooksHigh_LowTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage()
                .getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.PRICE_HIGH_TO_LOW);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.sort(ProductModel.getComparator(SortByDirection.PRICE_HIGH_TO_LOW));

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .asList()
                .isSorted()
                .isEqualTo(notebooksBeforeSort);
    }

    @Test
    public void addNotebookItemToCart() {
        String product = "Asus N551JK-XO076H Laptop";

        ProductModelCart productInfo = new NotebooksPage()
                .addNoteBookToCart(product)
                .waitForSuccessMessage()
                .closeSuccessMessage()
                .getMainMenu()
                .selectShoppingCart()
                .getProductInfo(product);
        assertThat(productInfo.productName())
                .isEqualTo(product);
        new CartPage().removeItem(product);
    }
}
