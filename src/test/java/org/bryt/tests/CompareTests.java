package org.bryt.tests;

import org.bryt.model.ProductModel;
import org.bryt.model.enums.SortByDirection;
import org.bryt.pages.base.BasePage;
import org.bryt.pages.content.computers.NotebooksPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CompareTests extends BaseTest {

    @BeforeClass
    public void beforeClass() {
        openUrl();
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();
    }

    @Test
    public void addNoteBooksToCompareList() {
        new BasePage()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks ");

        String note1 = "Asus N551JK-XO076H Laptop";
        String note2 = "Samsung Series 9 NP900X4C Premium Ultrabook";
        String note3 = "HP Spectre XT Pro UltraBook";

        List<ProductModel> productsInfo = new NotebooksPage()
                .addNoteBookToCompare(note1)
                .addNoteBookToCompare(note2)
                .addNoteBookToCompare(note3)
                .goToCompareList()
                .getProductsInfo();

        productsInfo.forEach(System.out::println);
    }

    @Test
    public void addNoteBooksToCompareList1() {
        new BasePage()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks ");

        List<String> noteBooksToCompare = List.of("Asus N551JK-XO076H Laptop",
                "Samsung Series 9 NP900X4C Premium Ultrabook",
                "HP Spectre XT Pro UltraBook");

        List<ProductModel> productsInfo = new NotebooksPage()
                .addNoteBooksToCompare(noteBooksToCompare)
                .getNotebooks();

        List<ProductModel> expectedProducts = new ArrayList<>();

        for (String name : noteBooksToCompare) {
            for (ProductModel productModel : productsInfo) {
                if (productModel.productName().equals(name)) {
                    expectedProducts.add(productModel);
                    break;
                }
            }
        }

        List<ProductModel> compared = new BasePage()
                .goToCompareList()
                .getProductsInfo();

        compared.sort(ProductModel.getComparator(SortByDirection.NAME_A_Z));
        expectedProducts.sort(ProductModel.getComparator(SortByDirection.NAME_A_Z));

        assertThat(compared)
                .asList()
                .isEqualTo(expectedProducts);
    }

}
