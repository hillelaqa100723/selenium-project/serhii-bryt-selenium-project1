package org.bryt.pages.login;

import org.bryt.driver.WebDriverHolder;
import org.bryt.pages.base.BasePage;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {


    @FindBy(id = "Email")
    private WebElement emailField;

    @FindBy(name = "Password")
    private WebElement passwordField;

    @FindBy(css = ".login-button")
    private WebElement loginButton;

    @FindBy(css = ".register-button")
    public WebElement registerButton;

    @FindBy(id = "RememberMe")
    private WebElement rememberMe;



    public BasePage login(String userEmail, String userPassword){
        enterDataToInput(emailField, userEmail);
        enterDataToInput(passwordField, userPassword);
        loginButton.click();
        return new BasePage();
    }

    public LoginPage loginIncorrect(String userEmail, String userPassword){
        enterDataToInput(emailField, userEmail);
        enterDataToInput(passwordField, userPassword);
        loginButton.click();
        return new LoginPage();
    }

    private void enterDataToInput(WebElement element, String value){
        element.clear();
        element.sendKeys(value);
    }

    public BasePage loginWithDefaultUser(){
        return login(PropertyReader.getInstance().getProperty("defaultUser"), PropertyReader.getInstance().getProperty("userPass"));
    }

}
