package org.bryt.pages.checkout;

import org.bryt.driver.WebDriverHolder;
import org.bryt.pages.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CheckoutBillingAddress extends CheckOutBasePage {

    //TODO add work with addresses

    public CheckoutBillingAddress setShipToTheSameAddress() {
        WebElement ch = WebDriverHolder.getInstance().getDriver().findElement(By.id("ShipToSameAddress"));
        if (!ch.isSelected()) {
            ch.click();
        }
        return this;
    }

    public CheckoutBillingAddress unSetShipToTheSameAddress() {
        WebElement ch = WebDriverHolder.getInstance().getDriver().findElement(By.id("ShipToSameAddress"));
        if (ch.isSelected()) {
            ch.click();
        }
        return this;
    }


}
