package org.bryt.step_defs;

import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import org.bryt.driver.WebDriverFactory;
import org.bryt.driver.WebDriverHolder;
import org.bryt.utils.PropertyReader;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseStepDef {

    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/"))
            WebDriverHolder.getInstance().getDriver().get(baseUrl + url);
        else WebDriverHolder.getInstance().getDriver().get(url);
    }

    protected void openUrl() {
        openUrl(PropertyReader.getInstance().getProperty("baseUrl"));
    }
}
