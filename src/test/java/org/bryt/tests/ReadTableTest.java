package org.bryt.tests;

import com.beust.ah.A;
import lombok.SneakyThrows;
import org.bryt.driver.WebDriverHolder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.json.Json;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ReadTableTest {

    @BeforeClass
    public void beforeClass(){
        WebDriverHolder.getInstance();
    }

    @AfterClass
    public void afterClass(){
        WebDriverHolder.getInstance().quitDriver();
    }

    @SneakyThrows
    @Test
    public void readTableTest() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        driver.navigate().to(new File("table_example.html").toURI().toURL());
        List<WebElement> elements = driver.findElements(By.xpath("//th"));
        List<String> headers = new ArrayList<>();

        for (WebElement element : elements) {
            headers.add(element.getText());
        }

        List<Map<String, String>> mapList = new ArrayList<>();

        List<WebElement> rows = driver.findElements(By.xpath("//tbody//tr"));
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.xpath(".//td"));
            List<String> cellsTexts = new ArrayList<>();

            for (WebElement cell : cells) {
                cellsTexts.add(cell.getText());
            }

            Map<String, String> stringStringMap = zipToMap(headers, cellsTexts);

            mapList.add(stringStringMap);
        }

        mapList.forEach(System.out::println);
    }

    @SneakyThrows
    @Test
    public void readTableTest1() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        driver.navigate().to(new File("table_example.html").toURI().toURL());

        String outerHTML = driver.findElement(By.xpath("//table")).getAttribute("outerHTML");

        Document document = Jsoup.parse(outerHTML);

        List<String> headers = document.selectXpath("//th").eachText();

        List<Map<String, String>> mapList = new ArrayList<>();

        Elements elements = document.selectXpath("//tbody//tr");
        for (Element element: elements){
            List<String> cellsTexts = element.select("td").eachText();
            Map<String, String> stringStringMap = zipToMap(headers, cellsTexts);
            mapList.add(stringStringMap);
        }

        for (Map<String, String> map: mapList){
            for (Map.Entry<String, String> entry:map.entrySet()){
                System.out.print(entry.getKey() + " ==> " + entry.getValue() + " | ");
            }
            System.out.println();
        }

    }


    static List<Integer> keys = List.of(1,2,3,4,5,6,7,8,9,10);
    static List<Integer> values = List.of(11,22,33,44,55,66,77,88,99,1010);

    public static void main(String[] args) {
        Map<Integer, Integer> integerIntegerMap = zipToMap(keys, values);
        System.out.println(integerIntegerMap);
    }

    public static <K, V> Map<K, V> zipToMap(List<K> keys, List<V> values) {
        return IntStream.range(0, keys.size()).boxed()
                .collect(Collectors.toMap(keys::get, values::get));
    }
}
