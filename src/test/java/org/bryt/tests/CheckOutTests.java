package org.bryt.tests;

import org.bryt.pages.base.BasePage;
import org.bryt.pages.checkout.CheckoutShippingMethod;
import org.bryt.pages.content.computers.NotebooksPage;
import org.testng.annotations.BeforeClass;

public class CheckOutTests extends BaseTest {
    @BeforeClass
    public void beforeClass(){
        openUrl();
        new BasePage()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks ", NotebooksPage.class)
                .addNoteBookToCart("HP Envy 6-1180ca 15.6-Inch Sleekbook")
                .getMainMenu()
                .selectShoppingCart()
                .agreeAndCheckout()
                .setShipToTheSameAddress()
                .save(CheckoutShippingMethod.class);

    }
}
