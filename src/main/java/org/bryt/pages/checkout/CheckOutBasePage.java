package org.bryt.pages.checkout;

import lombok.SneakyThrows;
import org.bryt.driver.WebDriverHolder;
import org.bryt.pages.base.BasePage;
import org.openqa.selenium.By;

public class CheckOutBasePage extends BasePage {


    @SneakyThrows
    public <T> T save(Class<T> pageToReturn) {
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.name("save"))
                .click();
        return pageToReturn.getConstructor().newInstance();
    }
}
