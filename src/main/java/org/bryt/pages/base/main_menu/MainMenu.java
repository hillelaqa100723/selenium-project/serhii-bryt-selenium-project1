package org.bryt.pages.base.main_menu;

import lombok.SneakyThrows;
import org.bryt.driver.WebDriverHolder;
import org.bryt.pages.cart.CartPage;
import org.bryt.pages.login.LoginPage;
import org.bryt.pages.my_account.MyAccountPage;
import org.bryt.pages.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainMenu {
    private void selectMenuItem(MenuItems menuItem) {
        findMenuItem(menuItem).click();
    }

    public LoginPage selectLogin() {
        selectMenuItem(MenuItems.LOG_IN);
        return new LoginPage();
    }

    public MyAccountPage selectMyAccount() {
        selectMenuItem(MenuItems.MY_ACCOUNT);
        return new MyAccountPage();
    }

    public BasePage selectLogout() {
        selectMenuItem(MenuItems.LOG_OUT);
        return new BasePage();
    }

    public Boolean isLogOutMenuItemVisible() {
        return findMenuItem(MenuItems.LOG_OUT)
                .isDisplayed();
    }

    public Boolean isMyAccountMenuItemVisible() {
        return findMenuItem(MenuItems.MY_ACCOUNT)
                .isDisplayed();
    }

    public Boolean isRegisterMenuItemVisible() {
        return findMenuItem(MenuItems.REGISTER).isDisplayed();
    }

    public Boolean isLoginMenuItemVisible() {
        return findMenuItem(MenuItems.LOG_IN).isDisplayed();
    }

    private WebElement findMenuItem(MenuItems menuItem) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        return driver
                .findElement(By.cssSelector(".ico-%s".formatted(menuItem.getValue())));

    }

    @SneakyThrows
    public CartPage selectShoppingCart() {
        selectMenuItem(MenuItems.SHOPPING_CART);
        WebDriverHolder.getInstance().getWait()
                .until(ExpectedConditions.urlContains("/cart"));
        Thread.sleep(2000);
        return new CartPage();
    }
}
