package org.bryt.step_defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.bryt.model.ProductModel;
import org.bryt.model.enums.SortByDirection;
import org.bryt.pages.base.BasePage;
import org.bryt.pages.compare.CompareListPage;
import org.bryt.pages.content.computers.NotebooksPage;
import org.bryt.pages.login.LoginPage;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompareStepDefs extends BaseStepDef {

    private List<ProductModel> addedItems = new ArrayList<>();
    private List<ProductModel> comparelist = new ArrayList<>();

    @Given("User navigates to Login page")
    public void userNavigatesToLoginPage() {
        openUrl();
        new BasePage().getMainMenu().selectLogin();
    }

    @And("User log in")
    public void userLogIn() {
        new LoginPage().loginWithDefaultUser();
    }

    @Given("User navigates to {string}")
    public void userNavigatesTo(String direction) {
        String[] split = direction.split("->");
        if (split.length == 1) {
            new BasePage().getNavigationMenu().selectNavigateMenuItem(split[0]);
        } else if (split.length == 2) {
            new BasePage().getNavigationMenu().selectNavigateMenuItemWithSubItem(split[0], split[1]);
        }
    }

    @And("User add to compare list {string}")
    public void userAddToCompareList(String itemName) {
        new NotebooksPage().addNoteBookToCompare(itemName);
        addedItems.add(new NotebooksPage().getNoteBookInfo(itemName));
    }

    @When("User go to compare list")
    public void userGoToCompareList() {
        new BasePage().goToCompareList();
    }

    @Then("At compare list should be:")
    public void atCompareListShouldBe(List<String> itemNames) {
        List<String> temp = new ArrayList<>(itemNames);
        comparelist = new CompareListPage().getProductsInfo();
        List<String> list = new ArrayList<>(comparelist
                .stream()
                .map(ProductModel::productName)
                .toList());

        temp.sort(Comparator.naturalOrder());
        list.sort(Comparator.naturalOrder());
        assertThat(list)
                .asList()
                .isEqualTo(temp);
    }
}
