package org.bryt.pages.base;

import lombok.SneakyThrows;
import org.bryt.driver.WebDriverHolder;
import org.bryt.pages.base.main_menu.MainMenu;
import org.bryt.pages.base.navigate_menu.NavigateMenu;
import org.bryt.pages.compare.CompareListPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class BasePage {
    @FindBy(id = "customerCurrency")
    private WebElement selectCurrency;

    @FindBy(css = ".bar-notification.success")
    private WebElement successMessage;

    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    public MainMenu getMainMenu(){
        return new MainMenu();
    }

    public NavigateMenu getNavigationMenu(){
        return new NavigateMenu();
    }

    public BasePage selectCurrency(Currencies currency){
        new Select(selectCurrency).selectByVisibleText(currency.getValue());
        return new BasePage();
    }

    @SneakyThrows
    protected void sleep(long ms){
        Thread.sleep(ms);
    }

    public BasePage waitForSuccessMessage(){
        WebDriverHolder.getInstance()
                .getWait()
                .until(ExpectedConditions.visibilityOf(successMessage));
        return this;
    }

    public BasePage closeSuccessMessage(){
        successMessage.findElement(By.cssSelector(".close")).click();
        return this;
    }

    public CompareListPage goToCompareList(){
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.cssSelector(".customer-service .list"))
                .findElement(By.xpath(".//a[text()='Compare products list']"))
                .click();
       return new CompareListPage();
    }

}
