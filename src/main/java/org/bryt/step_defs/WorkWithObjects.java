package org.bryt.step_defs;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;
import java.util.List;

public class WorkWithObjects {
    List<User> users = new ArrayList<>();

    @Given("We have list of users")
    public void weHaveListOfUsers(List<User> users) {
        this.users = users;
    }


    @When("Nothing to do")
    public void nothingToDo() {

    }

    @Then("Print data")
    public void printData() {
        users.forEach(System.out::println);
    }

    @Given("We have list of users from datatable")
    public void weHaveListOfUsersFromDatatable(DataTable dataTable) {
        List<List<String>> lists = dataTable.rows(1).asLists();
        for (List<String> list: lists){
            this.users.add(new User(list.get(0), list.get(1), Integer.parseInt(list.get(2))));
        }
    }
}
