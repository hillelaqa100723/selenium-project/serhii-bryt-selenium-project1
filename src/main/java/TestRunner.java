import io.cucumber.java.Before;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.bryt.driver.WebDriverFactory;
import org.bryt.driver.WebDriverHolder;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = {"src/test/resources/features"},
        glue = {"org.bryt.step_defs"},
        tags = "@sorting",
        plugin = {"pretty", "html:target/cucumber-reports/cucumberreport.html"}
)
public class TestRunner extends AbstractTestNGCucumberTests {

   @BeforeSuite
   public void beforeSuite() {
       WebDriverFactory.initDriver();
   }

   @AfterSuite
   public void afterSuite(){
       WebDriverHolder.getInstance().quitDriver();
   }

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
