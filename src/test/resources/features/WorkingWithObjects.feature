Feature: Working with objects

  Scenario: Use list of objects
    Given We have list of users
      | firstName | lastName  | age |
      | Ivan      | Ivanenko  | 31  |
      | Petro     | Petrenko  | 40  |
      | Fedir     | Fedorenko | 33  |
    When Nothing to do
    Then Print data

  Scenario: Use datatable
    Given We have list of users from datatable
      | firstName | lastName  | age |
      | Ivan      | Ivanenko  | 31  |
      | Petro     | Petrenko  | 40  |
      | Fedir     | Fedorenko | 33  |
    When Nothing to do
    Then Print data