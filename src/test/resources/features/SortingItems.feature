Feature: Sort items

  @sorting
  Scenario Outline: Sort direction check
    Given User navigates to Login page
    And User navigates to "Computers->Notebooks"
    When User choose sort direction "<sort_direction>"
    Then Products should be in "<sort_direction>"

    Examples:
      | sort_direction    |
      | NAME_A_Z          |
      | NAME_Z_A          |
      | PRICE_LOW_TO_HIGH |
      | PRICE_HIGH_TO_LOW |