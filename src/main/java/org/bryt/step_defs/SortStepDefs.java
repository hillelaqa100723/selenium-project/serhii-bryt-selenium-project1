package org.bryt.step_defs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.bryt.model.ProductModel;
import org.bryt.model.enums.SortByDirection;
import org.bryt.pages.content.computers.NotebooksPage;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SortStepDefs {

    private List<ProductModel> asIs = new ArrayList<>();
    private List<ProductModel> afterSorting = new ArrayList<>();
    @SneakyThrows
    @When("User choose sort direction {string}")
    public void userChooseSortDirection(String sortDirection) {
        asIs = new NotebooksPage().getNotebooks();
        new NotebooksPage().selectSortBy(SortByDirection.valueOf(sortDirection));
        Thread.sleep(2000);
        afterSorting = new NotebooksPage().getNotebooks();
    }

    @Then("Products should be in {string}")
    public void productsShouldBeIn(String sortDirection) {
        asIs.sort(ProductModel.getComparator(SortByDirection.valueOf(sortDirection)));
        assertThat(asIs)
                .asList()
                .isEqualTo(afterSorting);

    }
}
