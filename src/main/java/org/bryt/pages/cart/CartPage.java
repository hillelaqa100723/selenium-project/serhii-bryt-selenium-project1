package org.bryt.pages.cart;

import org.bryt.driver.WebDriverHolder;
import org.bryt.model.ProductModelCart;
import org.bryt.pages.base.BasePage;
import org.bryt.pages.checkout.CheckoutBillingAddress;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CartPage extends BasePage {

    @FindBy(css = "table.cart")
    private WebElement itemsTable;

    public List<ProductModelCart> getProductsFromTable() {
        List<ProductModelCart> list = new LinkedList<>();

        for (WebElement row : itemsTable.findElements(By.cssSelector("tbody tr"))) {
            ProductModelCart productModelCart = new ProductModelCart();
            String name = row.findElement(By.cssSelector(".product-name")).getText().trim();
            String priceAsText = row.findElement(By.cssSelector(".product-unit-price"))
                    .getText()
                    .substring(1)
                    .replaceAll(",", "");
            String qtyAsString = row.findElement(By.cssSelector(".qty-input")).getAttribute("value");

            productModelCart.productName(name);
            productModelCart.price(Double.parseDouble(priceAsText));
            productModelCart.qty(Integer.parseInt(qtyAsString));
            list.add(productModelCart);
        }
        return list;
    }

    public ProductModelCart getProductInfo(String itemName) {
        List<ProductModelCart> productsFromTable = getProductsFromTable();
        return productsFromTable
                .stream()
                .filter(productModelCart -> productModelCart.productName().equals(itemName))
                .findFirst()
                .get();
    }

    public CartPage removeItem(String itemName) {
        for (WebElement row : itemsTable.findElements(By.cssSelector("tbody tr"))) {
            if (row.findElement(By.cssSelector(".product-name")).getText().equals(itemName)) {
                row.findElement(By.cssSelector(".remove-btn")).click();
                return new CartPage();
            }
        }
        return new CartPage();
    }

    public CartPage agreeWithTheTermsOfService(){
        WebElement checkbox = WebDriverHolder.getInstance().getDriver().findElement(By.id("termsofservice"));
        if (!checkbox.isSelected()){
            checkbox.click();
        }
        return this;
    }

    public CheckoutBillingAddress agreeAndCheckout(){
        agreeWithTheTermsOfService();
        WebDriverHolder.getInstance().getDriver().findElement(By.id("checkout")).click();
        return new CheckoutBillingAddress();
    }

}
